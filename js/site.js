/**
 * @author Jackson Kwan
 */
"use strict";

document.addEventListener("DOMContentLoaded", setup);

    let global = {};

/**
 * Function ran when the DOM is completely loaded
 */
function setup(){
    global.wpm = document.querySelector('#interv');
    global.btn = document.querySelector('#btn');
    global.off = true;
    global.speed;
    global.prefoc = document.querySelector('#prefocus');
    global.foc = document.querySelector('#focus');
    global.postfoc = document.querySelector('#postfocus');
    loadSpeed();
    global.wpm.addEventListener("change", checkStep);
    global.btn.addEventListener("click", startStopInterval);
}

/**
 * Load speed from local storage
 */
function loadSpeed(){
    let json = localStorage.getItem('speed');
    if (json){
        global.speed = JSON.parse(json);
    }
    else {
        global.speed = 100;
    }
    global.wpm.value = global.speed;
}

/**
 * Calls getNextQuote to get the quote and modifies the button to do what it fits
 */
function startStopInterval(){
    if (global.off){
        global.btn.value = 'Start';
        clearInterval(global.speedInterv);
        global.off = false;
        
    }
    else {
        global.btn.value = 'Stop';
        getNextQuote();
        global.off = true;
    }
}

/**
 * Data validation for the speed input
 */
function checkStep(){
    if (global.wpm.value%50 != 0){
        global.wpm.value = global.speed;
    }
    else if (global.wpm.value > 1000 || global.wpm.value == 0){
        global.wpm.value = global.speed;
    }
    else {
        global.speed = global.wpm.value;
        localStorage.setItem('speed', JSON.stringify(global.speed));
    }
}

/**
 * Fetches from the API and returns a String array containing the quote split into each word
 * @param {*} e event in case it's needed
 */
function getNextQuote(e){
    fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
        .then(response => {
            if (!response.ok){
                throw new Error('Status code: ' + response.status);
            }
            return response.json();
        })
        .then(wholequote => {
            console.log(wholequote);
            return wholequote[0].split(' ');
        })
        .then(wordarr => displayquote(wordarr))
        .catch(error => displayerror(error));
}



/**
 * Creates the interval and calls displayword to show each word
 * @param {*} obj quote from API
 */
function displayquote(obj){
    let wpmspeed = 60000/global.wpm.value;
    global.speedInterv = setInterval(displayword, wpmspeed, obj);
    global.index = 0;
}

/**
 * Displays the word depending on its length. Focuses on a letter depending on length of the word
 * @param {*} obj String array of the quote split into words
 */
function displayword(obj){
        let word = obj[global.index];
        let prefochold = "";
        let fochold = "";
        let postfochold = "";
        if (word.length == 1){
            prefochold = '    ';
            fochold = word;
        }
        if (word.length >= 2 && word.length <= 5){
            prefochold = '   ' + word.substr(0,1);
            fochold = word.substr(1,1);
            postfochold = word.substr(2);
        }
        if (word.length >= 6 && word.length <= 9){
            prefochold = '  ' + word.substr(0,2);
            fochold = word.substr(2,1);
            postfochold = word.substr(3);
        }
        if (word.length >= 10 && word.length <= 13){
            prefochold = ' ' + word.substr(0,3);
            fochold = word.substr(3,1);
            postfochold = word.substr(4);
        }
        if (word.length > 13){
            prefochold = word.substr(0,4);
            fochold = word.substr(4,1);
            postfochold = word.substr(5);
        }
        global.prefoc.textContent = prefochold;
        global.foc.textContent = fochold;
        global.postfoc.textContent = postfochold;

    global.index++;
    if (global.index == obj.length){
        clearInterval(global.speedInterv);
        getNextQuote();
    }
}

/**
 * Displays an error and the error code if it ever happens
 * @param {*} err error returned from the click
 */
function displayerror(err){
    console.log('there was an error');
}

